# ptokaxinst

![](https://img.shields.io/badge/written%20in-bash-blue)

An installer for the DC hubsoft PtokaX.

- Installs dependencies
- Downloads, patches, and compiles PtokaX source
- Sets up restricted user and init.d script

Tested under Debian Wheezy (sysvinit)

Tags: nmdc


## Download

- [⬇️ ptokaxinst.sh](dist-archive/ptokaxinst.sh) *(3.03 KiB)*
