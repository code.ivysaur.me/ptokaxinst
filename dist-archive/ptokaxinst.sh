#!/bin/bash

set -e

SOURCE_DOWNLOAD=http://www.ptokax.org/files/0.5.0.2-nix-src.tgz
SOURCE_MD5=897d9793554b7e85e3ca84d9781cb670
INSTALL_DIR=/opt/PtokaX/
GENERATE_USERNAME=ptokax

fatal() {
	echo "FATAL: $1" 1>&2
	exit 1
}

dependencies() {
	# base64 and md5sum are in coreutils
	for dep in build-essential wget liblua5.2 liblua5.2-dev zlib1g zlib1g-dev authbind procps ; do
		if [[ $UID != 0 ]] ; then
			fatal "Need root to install $dep"
		fi
		
		if ! dpkg -s "$dep" > /dev/null 2>&1 ; then
			apt-get -y install "$dep"
		fi
	done
}

download() {
	local dest=$(mktemp)
	wget "$SOURCE_DOWNLOAD" -O "$dest"
	
	local got_hash=$(md5sum "$dest" | cut -d' ' -f1)
	if [[ $got_hash != $SOURCE_MD5 ]] ; then
		fatal "MD5 Mismatch"
	fi
	
	mkdir -p "${INSTALL_DIR}"
	
	cd "${INSTALL_DIR}"
	tar zxf "$dest"
	
	rm "$dest"
	
	mv PtokaX build
	cd build
	
	sed -i 's/lua5.1/lua5.2/' makefile
	sed -i 's/lua51/lua52/' makefile
	
	make -j4
	
	rm -r core skein obj tinyxml ico
	rm makefile compile.txt
	
	cd ../
	mv build/* .
	rmdir build
	
	cp -R cfg.example cfg
	sed -i 's/AutoRegister">1/AutoRegister">0/' cfg/Settings.xml
	sed -i -r 's/HubAddress">.+</HubAddress">0.0.0.0</' cfg/Settings.xml
	
	mkdir service
	touch service/stdout
	touch service/stderr
	
}

genpass() {
	dd if=/dev/urandom bs=12 count=1 | base64
}

create_user() {
	useradd "$GENERATE_USERNAME" -p $(genpass)
}

create_dir() {
	mkdir -p "$INSTALL_DIR"
	chown "${GENERATE_USERNAME}:${GENERATE_USERNAME}" "$INSTALL_DIR"
	chmod 750 "$INSTALL_DIR"
}

allow_bind() {
	mkdir -p /etc/authbind/byport
	touch /etc/authbind/byport/411
	chown "${GENERATE_USERNAME}:root" /etc/authbind/byport/411
	chmod 700 /etc/authbind/byport/411
}

build_init_script() {
	cat <<EOF
#!/bin/bash

### BEGIN INIT INFO
# Provides:          ptokax
# Required-Start:    \$network \$named \$local_fs \$remote_fs
# Required-Stop:     \$network \$named \$local_fs \$remote_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: PtokaX
# Description:       PtokaX
### END INIT INFO

. /lib/lsb/init-functions

case "\$1" in
	start)
		su "$GENERATE_USERNAME" \\
			-c 'cd "$INSTALL_DIR" && authbind ./PtokaX' \\
			>"${INSTALL_DIR}service/stdout" \\
			2>"${INSTALL_DIR}service/stderr" \\
			&
		exit 0
	;;
	restart|force-reload)
		/etc/init.d/ptokax stop
		/etc/init.d/ptokax start
		exit 0
	;;
	stop)
		pkill PtokaX
		exit 0
	;;
	status)
		pidof PtokaX >/dev/null
		exit $?
	;;
	*)
		echo "Usage: /etc/init.d/ptokax {start|stop|restart|status}"
		exit 2
	;;
esac

EOF
}

install_init_script() {
	build_init_script > /etc/init.d/ptokax
	chown root:root /etc/init.d/ptokax
	chmod 755 /etc/init.d/ptokax
	
	update-rc.d ptokax defaults
}

main() {
	
	if [[ $USER == "root" ]] ; then
		if [[ "$BASH_SOURCE" == "" ]] ; then
			fatal "Running from pipe is unsupported"
		fi

		dependencies	
		create_user
		create_dir
		
		cat "$BASH_SOURCE" | su "$GENERATE_USERNAME" -c "bash -s"
		
		allow_bind
		install_init_script
		
		service ptokax start
		
	elif [[ $USER == $GENERATE_USERNAME ]] ; then
	
		download
	
	else
		fatal "Please run as root"
	
	fi
	exit 0
}

main
